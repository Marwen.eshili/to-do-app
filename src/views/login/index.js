import React from "react";
import { Button, Card, Grid, TextField } from "@mui/material";
import SocialIcons from "./components/socialIcons";
import { FaFacebookF, FaGoogle, FaLinkedinIn } from "react-icons/fa";
import LoginForm from "./components/loginForm";

const icons = [FaFacebookF, FaGoogle, FaLinkedinIn];

export default function Login() {
  return (
    <div>
      <Grid
        container
        alignItems="center"
        justifyContent="center"
        className="section_login"
      >
        <Grid item xs={12} md={6}>
          <Card className="card_login">
            <Grid
              item
              xs={12}
              container
              alignItems="center"
              justifyContent="center"
              className="section_left"
            >
              <Grid item xs={11}>
                <h3 className="title_login">sign in</h3>
                <div className="items_login">
                  {icons.map((el, i) => (
                    <SocialIcons key={i} icon={el} />
                  ))}
                </div>
                <p>Or use your account</p>
                <div className="input_login">
                  <LoginForm />
                </div>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}
