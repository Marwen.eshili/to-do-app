import { FaGoogle } from "react-icons/fa";

const SocialIcon = (props) => {
  return (
    <div className="icon_login">
      <props.icon></props.icon>
    </div>
  );
};

export default SocialIcon;
