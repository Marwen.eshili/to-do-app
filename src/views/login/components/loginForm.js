import { useRef, useState, useEffect, useContext } from "react";
import { Grid, TextField, Button } from "@mui/material";
import { useFormik } from "formik";
import * as Yup from "yup";
// import AuthContext from "../../../context/AuthProvider";
// import axios from "../../../utils/axios";
import { useHistory } from "react-router-dom";
// import useAuth from "../../../hooks/useAuth";
// import { setSession } from "../../../context/AuthProvider";
import Btn from "../../../components/button/btn";

const LOGIN_URL = process.env.REACT_APP_API_URL + "/users/login";

const LoginForm = (props) => {
  const history = useHistory();
  //   const { login } = useAuth();
  const [errMsg, setErrMsg] = useState("");
  const [success, setSuccess] = useState(false);
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().email().required("email is required"),
      password: Yup.string().required("password is required"),
    }),
    onSubmit: async (values) => {
      try {
        console.log(values);
        // const res = await login(values.email, values.password);
        // const response = await axios.post(`/users/login`, values);
        // let { token } = response.data;
        // let { role } = response.data.data.user;
        // setSession(token, role);
        // console.log(response);
        // setSuccess(true);
        setErrMsg("");
        // login();
        // history.push("/");
      } catch (err) {
        console.log(err);
        setErrMsg(err.message);
      }
    },
  });
  return (
    <Grid
      container
      xs={12}
      spacing={2}
      alignItems="center"
      justifyContent="center"
      component="form"
      onSubmit={formik.handleSubmit}
    >
      <Grid item xs={12}>
        <TextField
          error={Boolean(formik.touched.email && formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
          fullWidth
          name="email"
          type="text"
          variant="outlined"
          label="email"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.email}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          error={Boolean(formik.touched.password && formik.errors.password)}
          helperText={formik.touched.password && formik.errors.password}
          fullWidth
          name="password"
          type="password"
          variant="outlined"
          label="password"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.password}
        />
      </Grid>

      <Btn color="#1b2026" type="submit">
        sign in
      </Btn>
    </Grid>
  );
};

export default LoginForm;
