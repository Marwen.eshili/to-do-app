import { createSlice, current } from "@reduxjs/toolkit";

const initialState = {
  isSidebarOpened: false,
  item: "Dashboard",
  subItem: "",
};

export const settingsSlice = createSlice({
  name: "settings",
  initialState,
  reducers: {
    toggleSidebar: (state) => {
      state.isSidebarOpened = !current(state).isSidebarOpened;
    },
    openSidebar: (state) => {
      state.isSidebarOpened = true;
    },
    closeSidebar: (state) => {
      state.isSidebarOpened = false;
    },
    changeItem: (state, { payload }) => {
      const { name, subMenus } = payload;
      state.item = name;
      if (subMenus.length === 0) {
        state.subItem = "";
      }
    },
    changeSubItem: (state, { payload }) => {
      state.subItem = payload;
    },
  },
});

export const {
  toggleSidebar,
  openSidebar,
  closeSidebar,
  changeItem,
  changeSubItem,
} = settingsSlice.actions;

export default settingsSlice.reducer;
