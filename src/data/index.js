import { configureStore } from "@reduxjs/toolkit";
// import exampleReducer from "./slices/exampleSlice";
// import authReducer from "./slices/authSlice";
import settingsReducer from "./slices/settings";

export const store = configureStore({
  reducer: {
    // example: exampleReducer,
    // auth: authReducer,
    settings: settingsReducer,
  },
  //   devTools: import.meta.env.ENABLE_REDUX_DEV_TOOLS,
});
