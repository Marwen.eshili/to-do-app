import { Button, Grid } from "@mui/material";

const Btn = (props) => {
  const { color, type } = props;

  const getColor = (color) => {
    if (color === "transparent" || color === "#1b2026") return "white";
    else return "black";
  };
  return (
    <button
      onClick={props.onClick}
      className="btn"
      style={{ backgroundColor: color, color: getColor(color) }}
      type={type === "submit" ? "submit" : "button"}
    >
      {props.children}
    </button>
  );
};

export default Btn;
