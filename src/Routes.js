import React, { Fragment, lazy, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { Helmet } from "react-helmet";
import Login from "./views/login";
export const RenderRoutes = ({ routes }) => {
  let user = {};
  return (
    <Suspense>
      <Switch>
        {routes.map((route, i) => {
          const Layout = route.layout || Fragment;
          const Component = route.component;
          return (
            <Route
              key={i}
              path={route.path}
              exact={route.exact}
              render={(props) => (
                <Layout>
                  <Helmet>
                    <title>{route.title}</title>
                  </Helmet>
                  {route.routes ? (
                    <RenderRoutes routes={route.routes} />
                  ) : user ? (
                    <Component {...props} />
                  ) : (
                    <Login />
                  )}
                </Layout>
              )}
            />
          );
        })}
      </Switch>
    </Suspense>
  );
};
const routes = [
  {
    path: "/",
    routes: [
      {
        exact: true,
        path: "/",
        component: lazy(() => import("./views/HomePage.js")),
      },
      {
        exact: true,
        path: "/login",
        component: lazy(() => import("./views/login")),
      },
      {
        exact: true,
        path: "/contents/projects",
        component: lazy(() => import("./views/courses")),
      },
      {
        exact: true,
        path: "/contents/profile",
        component: lazy(() => import("./views/profile/Profile")),
      },
      //   {
      //     exact: true,
      //     path: "/projectDetails/:id",
      //     component: lazy(() =>
      //       import("./views/projects/ProjectDetails/projectDetails")
      //     ),
      //     title: "Project Details",
      //   },
      //   {
      //     exact: true,
      //     path: "/projects",
      //     component: lazy(() => import("./views/projects/projects")),
      //     title: "Projects",
      //   },
    ],
  },
  {
    path: "*",
    routes: [
      //   {
      //     component: () => <Redirect to="/404" />,
      //     title: 'TA|Not found',
      //   },
    ],
  },
];

export default routes;
