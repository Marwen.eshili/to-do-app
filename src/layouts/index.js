// import { Outlet } from "react-router-dom";
import Backdrop from "../components/backdrop";
import SideMenu from "./sideBar/SideMenu";
import Navbar from "./navbar/navbar";
import { useState } from "react";
import useWindowSize from "../hooks/useWindowSize";
import { useSelector, useDispatch } from "react-redux";

const MainLayout = ({ onCollapse, inactive }) => {
  const { width } = useWindowSize();
  const { isSidebarOpened } = useSelector((state) => state.settings);

  return (
    <div className="main_layout">
      {width < 800 && <Navbar />}
      <Navbar inactive={inactive} />
      <Backdrop />
      {(isSidebarOpened === true || width > 800) && (
        <div className="main_layout__container">
          {/* <Outlet /> */}
          <SideMenu onCollapse={onCollapse} />
        </div>
      )}
    </div>
  );
};

export default MainLayout;
