import React, { useRef, useState } from "react";
import { NavLink, Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  toggleSidebar,
  closeSidebar,
  changeItem,
  changeSubItem,
} from "../../data/slices/settings";

/**
 * @author
 * @function MenuItem
 **/

const MenuItem = (props) => {
  const dispatch = useDispatch();

  const { name, subMenus, iconClassName, onClick, to, exact } = props;
  const [expand, setExpand] = useState(false);
  return (
    <li onClick={props.onClick}>
      <Link
        exact
        to={to}
        onClick={() => {
          setExpand((e) => !e);
          dispatch(changeItem({ name, subMenus }));
        }}
        className={`menu-item`}
        style={{ color: "white" }}
      >
        <div className="menu-icon">
          {/* <i class={iconClassName}></i> */}
          {iconClassName}
        </div>
        <span style={{ color: "white" }}>{name}</span>
      </Link>
      {subMenus && subMenus.length > 0 ? (
        <ul className={`sub-menu`}>
          {subMenus.map((menu, index) => (
            <li key={index}>
              <NavLink
                style={{ color: "white" }}
                to={menu.to}
                onClick={() => {
                  dispatch(closeSidebar());
                  dispatch(changeSubItem(menu?.name));
                }}
              >
                {menu.name}
              </NavLink>
            </li>
          ))}
        </ul>
      ) : null}
    </li>
  );
};

export default MenuItem;
