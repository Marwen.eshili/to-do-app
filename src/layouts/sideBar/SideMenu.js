import React, { useEffect, useState } from "react";
import logo from "../../assets/logo/webscript.png";
import userPng from "../../assets/profile.jpeg";
import {
  FaHome,
  FaBars,
  FaUserAlt,
  FaAngleRight,
  FaSearch,
  FaProjectDiagram,
} from "react-icons/fa";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import MenuItem from "./MenuItem";
import "./sidbar.scss";
import Navbar from "../navbar/navbar";
import useWindowSize from "../../hooks/useWindowSize";
import { toggleSidebar } from "../../data/slices/settings";
import { useSelector, useDispatch } from "react-redux";

/**
 * @author
 * @function SideMenu
 **/

export const menuItems = [
  {
    name: "Dashboard",
    exact: true,
    to: "/",
    iconClassName: <FaHome />,
  },
  {
    name: "Projects",
    exact: true,
    to: `/contents/projects`,
    iconClassName: <FaProjectDiagram />,
  },
  { name: "Login", to: `/login`, iconClassName: <FaUserAlt /> },
  {
    name: "Content",
    exact: true,
    to: `/content`,
    iconClassName: <FaHome />,
    subMenus: [
      { name: "Courses", to: "/content-2/courses" },
      { name: "Videos", to: "/content-2/videos" },
    ],
  },
  {
    name: "Profile",
    exact: true,
    to: "/contents/profile",
    iconClassName: <FaUserAlt />,
  },
];

const SideMenu = (props) => {
  const { width } = useWindowSize();
  const { isSidebarOpened } = useSelector((state) => state.settings);
  // let inactive = isSidebarOpened;
  const dispatch = useDispatch();

  const [inactive, setInactive] = useState(false);

  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  useEffect(() => {
    if (inactive) {
      removeActiveClassFromSubMenu();
    }
    props.onCollapse(inactive);
  }, [inactive]);

  const removeActiveClassFromSubMenu = () => {
    document.querySelectorAll(".sub-menu").forEach((el) => {
      el.classList.remove("active");
    });
  };

  useEffect(() => {
    let menuItems = document.querySelectorAll(".menu-item");
    menuItems.forEach((el) => {
      el.addEventListener("click", (e) => {
        const next = el.nextElementSibling;
        removeActiveClassFromSubMenu();
        menuItems.forEach((el) => el.classList.remove("active"));
        el.classList.toggle("active");
        if (next !== null) {
          next.classList.toggle("active");
        }
      });
    });
  }, []);

  // if (width < 800) {
  //   return <Navbar />;
  // }

  return (
    <>
      {/* {width < 800 && <Navbar />} */}
      {/* {(isSidebarOpened === true || width > 800) && ( */}
      <div className={`side-menu ${inactive ? "inactive" : ""} `}>
        <div className="top-section">
          <span>
            <div className="logo">
              <img src={logo} alt="webscript" />
            </div>
            {inactive === false && <h4>My To Do</h4>}
          </span>
          <div
            onClick={() => setInactive(!inactive)}
            className="toggle-menu-btn"
          >
            {inactive ? (
              <i class="bi bi-arrow-right-square-fill"></i>
            ) : (
              <i class="bi bi-arrow-left-square-fill"></i>
            )}
          </div>
        </div>

        <div className="search-controller">
          <button className="search-btn">
            <i class="bi bi-search"></i>
          </button>

          <input type="text" placeholder="search" />
        </div>

        <div className="divider"></div>

        <div className="main-menu">
          <ul>
            {menuItems.map((menuItem, index) => (
              <MenuItem
                key={index}
                name={menuItem.name}
                exact={menuItem.exact}
                to={menuItem.to}
                subMenus={menuItem.subMenus || []}
                iconClassName={menuItem.iconClassName}
                onClick={(e) => {
                  if (inactive) {
                    setInactive(true);
                  }
                }}
              />
            ))}

            {/* <li>
            <a className="menu-item">
              <div className="menu-icon">
                <i class="bi bi-speedometer2"></i>
              </div>
              <span>Dashboard</span>
            </a>
          </li>
          <MenuItem
            name={"Content"}
            subMenus={[{ name: "Courses" }, { name: "Videos" }]}
          />
          <li>
            <a className="menu-item">
              <div className="menu-icon">
                <i class="bi bi-vector-pen"></i>
              </div>
              <span>Design</span>
            </a>
          </li> */}
          </ul>
        </div>

        <div className="side-menu-footer">
          <div className="avatar">
            <img src={userPng} alt="user" />
          </div>
          <div className="user-info">
            <h5>Marwen Shili</h5>
            <p>marwen@gmail.com</p>
          </div>
        </div>
      </div>
      {/* )} */}
    </>
  );
};

export default SideMenu;
