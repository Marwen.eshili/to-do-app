import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { FaBars, FaBell } from "react-icons/fa";
import { CgClose } from "react-icons/cg";
import { MdNotifications } from "react-icons/md";
import { toggleSidebar } from "../../data/slices/settings";
import { useDispatch, useSelector } from "react-redux";
import useWindowSize from "../../hooks/useWindowSize";
import logo from "../../assets/logo/webscript.png";

export default function Navbar({ inactive }) {
  const dispatch = useDispatch();
  const { width } = useWindowSize();
  const { isSidebarOpened, item, subItem } = useSelector(
    (state) => state.settings
  );

  return (
    <div className="app-bar">
      <Box>
        <AppBar elevation={0} className="app_bar_content">
          <Toolbar className="items_bar">
            {/* <div className="btn_bars" onClick={() => dispatch(toggleSidebar())}>
              <FaBars color="#999999" />
            </div> */}
            {width < 800 && (
              <div className="s_logo">
                {width < 800 && <img src={logo} alt="webscript" />}
                {!isSidebarOpened && <h4>MyToDo</h4>}
              </div>
            )}
            <div></div>
            {width > 800 ? (
              <div className="profile_avatar">
                {/* <img src={profile}></img> */}
                <FaBell color="#999999" />
              </div>
            ) : (
              <div
                className="btn_bars"
                onClick={() => dispatch(toggleSidebar())}
              >
                {!isSidebarOpened ? (
                  <FaBars color="#999999" />
                ) : (
                  <CgClose color="#999999" />
                )}
              </div>
            )}
            {width > 800 && (
              <div className={`item_nav_name ${inactive ? "inactive" : ""} `}>
                <h4>
                  {item} {` > ${subItem}`}
                </h4>
              </div>
            )}
          </Toolbar>
        </AppBar>
      </Box>
    </div>
  );
}
