import React from "react";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import routes, { RenderRoutes } from "./Routes";
import SideMenu from "./layouts/sideBar/SideMenu";
import { useState } from "react";
import MainLayout from "./layouts";

const browserHistory = createBrowserHistory();

const App = () => {
  const [inactive, setInactive] = useState(true);
  return (
    <Router history={browserHistory}>
      <MainLayout
        onCollapse={(inactive) => {
          setInactive(inactive);
        }}
        inactive={inactive}
      />
      <div className={`container ${inactive ? "inactive" : ""}`}>
        <RenderRoutes routes={routes} />
      </div>
    </Router>
  );
};

export default App;
