import Title from "../../components/title";
import TaskSummary from "./components/taskSummary";

const HomePage = () => {
  return (
    <div className="section_home">
      <div className="s_overview">
        <Title text="Task Summary" />
        <TaskSummary />
      </div>
      <div className="s_projects">
        <Title text="Your Projects" />
      </div>
    </div>
  );
};
export default HomePage;
